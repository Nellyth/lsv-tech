from django.contrib.auth.models import User, Group
from rest_framework import serializers


class user_serializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
        ]


class group_serialize(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = [
            ''
        ]