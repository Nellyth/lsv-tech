from django.apps import AppConfig


class AppsapiConfig(AppConfig):
    name = 'Apps'
