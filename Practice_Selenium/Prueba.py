__author__ = 'Nellyth Arroyo Marrugo'
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import Practice_Selenium.Datos as Datos
postsUrls = []
count_likes=0
count_views=0
browser = webdriver.Firefox(executable_path='/home/nellyth/Descargas/geckodriver-v0.24.0-linux64/geckodriver')


def login(username,password):
    browser.get('https://www.instagram.com/accounts/login/')
    time.sleep(1)

    emailInput = browser.find_elements_by_css_selector('form input')[0]
    passwordInput = browser.find_elements_by_css_selector('form input')[1]

    emailInput.send_keys(username)
    passwordInput.send_keys(password)
    passwordInput.send_keys(Keys.ENTER)
    time.sleep(2)

    browser.get('https://www.instagram.com/apax.13/?hl=es-la')

    SCROLL_PAUSE_TIME = 2
    last_height = browser.execute_script("return document.body.scrollHeight")
    while True:
        print("Scrolling..............")
        path = browser.find_elements_by_xpath("//*[@class='v1Nh3 kIKUG  _bz0w']//a")
        getPostUrls(path)
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")  # Scroll down to bottom
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = browser.execute_script("return document.body.scrollHeight")

        if new_height == last_height:
            getPost()
            return
        last_height = new_height


def getPost():
    global count_likes
    global count_views
    for url in postsUrls:
        browser.get(url)
        path = browser.find_elements_by_xpath("//*[@class='zV_Nj']")
        if path:
            for p in path:
                likes = p.get_attribute("innerText")
                count_likes+=int(likes.split(' ')[0])
            time.sleep(3)
        else:
            path = browser.find_elements_by_xpath("//*[@class='vcOH2']")
            for p in path:
                views = p.get_attribute("innerText")
                count_views+=int(views.split(' ')[0])


def getPostUrls(path):
    print("\nRetriving posts url .......")
    for p in path:
        url = p.get_attribute("href")
        if url not in postsUrls:
            postsUrls.append(url)
    print("Total posts found = " + str(len(postsUrls)))

login(Datos.username, Datos.password)
print('Likes Totales: {}'.format(count_likes))
print('Views Totales: {}'.format(count_views))

